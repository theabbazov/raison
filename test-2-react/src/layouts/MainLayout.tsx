import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

const MainLayout = () => {
    return (
        <>
            <header className="h-20 bg-primary flex items-center p-4">
                <h1 className="text-3xl text-black">Title</h1>
            </header>
            <main className="flex flex-col p-4 h-full">
                <Outlet />
            </main>
        </>
    )
}

export default MainLayout