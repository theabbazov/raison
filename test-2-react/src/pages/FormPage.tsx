import { useCallback, useEffect, useRef, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import FormCheckbox from "../components/FormCheckbox";
import FormInput from "../components/FormInput";
import { emailValidator } from "../utils";

export default function FormPage() {
  const navigate = useNavigate()
  const { hash } = useLocation()
  const intervalRef = useRef<ReturnType<typeof setInterval> | null>(null)
  const holdTime = useRef<number>(500)


  const [email, setEmail] = useState<string>('');
  const [isAgree, setIsAgree] = useState<boolean>(false)

  const [isValid, setIsValid] = useState<boolean>(false)


  useEffect(() => {
    if (emailValidator(email) && isAgree) {
      setIsValid(true)
      navigate(`#${email}`)
      return
    }
    setIsValid(false)
    navigate(``)
  }, [email, isAgree])

  useEffect(() => {
    if (hash) {
      setEmail(hash.replace('#', ''))
      setIsAgree(true)
    }
    return () => {
      if (intervalRef.current) clearInterval(intervalRef.current)
    }
  }, [])

  const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {
    navigate(`/navigate/${email}`)
  }

  const handleEmail = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value)
  }, [])

  const handleAgree = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setIsAgree(e.target.checked)
  }, [])

  const startCounter = () => {
    if (intervalRef.current) return;
    intervalRef.current = setInterval(() => {
      holdTime.current -= 10
      if (!holdTime.current) {
        navigate(`${email}`)
      }
    }, 10);
  };

  const stopCounter = () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
      holdTime.current = 500
    }
  };

  return (
    <>
      <FormInput value={email} onChange={handleEmail} />

      <FormCheckbox type="checkbox" checked={isAgree} onChange={handleAgree} />
      <button
        disabled={!isValid}
        className="btn btn-primary mt-auto"
        onMouseDown={startCounter}
        onMouseUp={stopCounter}
      >
        Hold to proceed
      </button>
    </>
  )
}