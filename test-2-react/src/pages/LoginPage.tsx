import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import FormInput from '../components/FormInput'
import { login } from '../utils'
import Popup from '../components/Popup'



const LoginPage = () => {
    const { email } = useParams<{ email: string }>()
    if (!email) return
    const navigate = useNavigate()
    const [responseText, setResponse] = useState<'success!!!' | 'error!' | ''>('')
    const [isLoading, setIsLoading] = useState(false)

    const handleSubmit = async () => {
        setIsLoading(true)
        await login(email)
            .then(
                () => setResponse('success!!!'),
                () => setResponse('error!')
            )
            .finally(() => (document.getElementById('my_modal') as HTMLDialogElement).showModal())
        setIsLoading(false)
    }

    console.log(responseText)

    const goBack = () => navigate(-1)

    return (
        <>
            <Popup >
                {responseText}
            </Popup>
            <FormInput value={email} disabled />
            <div className='flex gap-3 mt-auto'>
                <button onClick={goBack} className='btn btn-square flex-1 w-2/4'>Back</button>

                <button onClick={handleSubmit} className='btn btn-primary flex-1 w-2/4'>
                    {isLoading && <span className="loading loading-spinner"></span>}
                    Confirm
                </button>
            </div>
        </>
    )
}

export default LoginPage