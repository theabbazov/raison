import { memo } from "react"

interface Props extends React.InputHTMLAttributes<HTMLInputElement> { }

function FormInput({ ...other }: Props) {
    return (
        <label className="form-control">
            <div className="label">
                <span className="label-text">Email</span>
            </div>
            <input type="text" placeholder="Type here" className="input" {...other} />
            {/* <div className="label">
                <span className="label-text-alt">Helper text</span>
            </div> */}
        </label>
    )
}
export default memo(FormInput)