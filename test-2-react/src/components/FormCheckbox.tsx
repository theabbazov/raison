import { memo } from "react"

interface Props extends React.InputHTMLAttributes<HTMLInputElement> { }

 function FormCheckbox({...other}: Props) {
    return (
        <div className="form-control">
            <label className="label cursor-pointer justify-start gap-2">
                <input type="checkbox" className="checkbox checkbox-primary" {...other}/>
                <span className="label-text">I agree</span>
            </label>
        </div>
    )
}

export default memo(FormCheckbox)