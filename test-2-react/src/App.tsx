import { BrowserRouter, Routes, useRoutes, Route, Navigate } from 'react-router-dom'
import MainLayout from './layouts/MainLayout'
import FormPage from './pages/FormPage'
import LoginPage from './pages/LoginPage'

export default function App() {

    return (
        <>
            <Routes>
                <Route path='/' element={<MainLayout />} >
                    <Route index element={<Navigate to='login'/>}/>
                    <Route path='/login' index element={<FormPage />} />
                    <Route path='/login/:email' element={<LoginPage />} />
                </Route>
            </Routes>
        </>
    )
}






